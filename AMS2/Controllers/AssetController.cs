﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AMS2.Models;
using AMS2.Models.Interface;
using AMS2.ViewModels;
using NLog;
using AMS.Attr;
using System.Collections;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
//using LinqToExcel;
using NPOI.XSSF.UserModel;
using System.Data.OleDb;
using System.Data;
using NPOI.HSSF.UserModel;
using System.Web.UI.HtmlControls;

namespace AMS.Controllers
{
    public class AssetController : Controller
    {
        AMS2.Models.AMSEntities db;
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IRepo<AMS_Main> AssetRepo;
        private IRepo<AMS_MainLog> LogRepo;
        private IRepo<vAMS_Owner> OwnerRepo;
        private IRepo<AMS_OwnerGroup> GroupRepo;
        private IRepo<AMS_Location> LocationRepo;
        private string ErrMsg = "{0}\n{1}";

        public AssetController() 
        {
            this.AssetRepo = new GenericRepo<AMS_Main>();
            this.LogRepo = new GenericRepo<AMS_MainLog>();
            this.OwnerRepo = new GenericRepo<vAMS_Owner>();
            this.GroupRepo = new GenericRepo<AMS_OwnerGroup>();
            this.LocationRepo = new GenericRepo<AMS_Location>();
            
        }

        //這是原本.net MVC舊版本的部分，現在不用這個了。
        public ActionResult oldGrid(string fn, string sort)
        {

            var vm = AssetRepo.GetAll();
            if (fn == "EmpName" && sort == "desc") { vm = vm.OrderByDescending(x => x.EmpName); }
            if (fn == "EmpName" && sort == "asc") { vm = vm.OrderBy(x => x.EmpName); }

            if (fn == "OwnerGroupID" && sort == "desc") { vm = vm.OrderByDescending(x => x.OwnerGroupID); }
            if (fn == "OwnerGroupID" && sort == "asc") { vm = vm.OrderBy(x => x.OwnerGroupID); }

            if (fn == "IsCheck" && sort == "desc") { vm = vm.OrderByDescending(x => x.IsChecked).ThenByDescending(x => x.EmpName); }
            if (fn == "IsCheck" && sort == "asc") { vm = vm.OrderBy(x => x.IsChecked).ThenBy(x => x.EmpName); }

            if (fn == "IsMark" && sort == "desc") { vm = vm.OrderByDescending(x => x.IsMarked).ThenByDescending(x => x.EmpName); }
            if (fn == "IsMark" && sort == "asc") { vm = vm.OrderBy(x => x.IsMarked).ThenBy(x => x.EmpName); }

            if (fn == "AssetID" && sort == "desc") { vm = vm.OrderByDescending(x => x.AssetID); }
            if (fn == "AssetID" && sort == "asc") { vm = vm.OrderBy(x => x.AssetID); }

            if (fn == "AssetName" && sort == "desc") { vm = vm.OrderByDescending(x => x.AssetName); }
            if (fn == "AssetName" && sort == "asc") { vm = vm.OrderBy(x => x.AssetName); }

            return View(vm.ToList());
        }

        //什麼都不做，只吐出乾淨的頁面!!
        public ActionResult Grid(string Typ)
        {
            if (Typ == "IT" || Typ == "Fixed") { } else { return RedirectToAction("WhichGridTyp"); }
            ViewData["Typ"] = Typ;
            return View("Grid2"); //Grid2.cshtml
        }


        /// <summary>
        /// 提供給Grid的頁面的ajax資料來源
        /// </summary>
        /// <returns></returns>
        [AllowCrossSiteJsonAttribute]
        [HttpPost]
        public JsonResult GridAjax(string Typ)
        {
            //var vm = AssetRepo.GetAll();
            using (db = new AMSEntities())
            {
                var vm = db.AMS_Main.Where(x => x.Typ == Typ && x.IsEnable==null).ToList();
                var MaxJson = Json(vm);
                    MaxJson.MaxJsonLength = 999999999;
                return MaxJson;
            }
        }

        [HttpPost]
        public JsonResult SaveAsset(AMS_Main asset) {
            using (db = new AMSEntities())
            {
                string msg = "Done.";
                try
                {
                    var e = db.AMS_Main.Where(x => x.AssetID == asset.AssetID).SingleOrDefault();
                    if (e != null)
                    {
                        e.Note = asset.Note;
                        e.UpdateTime = DateTime.Now;
                        db.SaveChanges();
                    }
                    else {
                        msg = "Can't find this asset.";
                    }
                }
                catch (Exception ex) {
                    msg=ex.Message.ToString();
                }
                return Json(msg);
            }
        }

        public ActionResult MobileSet(string Typ) 
        {
            ViewData["Typ"] = Typ;
            return View();
        }

        [HttpPost]
        public ActionResult MobileQuery(string Typ, string AssetID)
        {
            if (Typ == "IT" || Typ == "Fixed") { } else { return RedirectToAction("WhichTyp"); }
            if (AssetID == "") return RedirectToAction("MobileSet", new { Typ = Typ });
            return View("MobileSet", this.GetAssetViewModel(Typ, AssetID));
        }

        [HttpGet]
        public ActionResult MobileQuery(string Typ, string AssetID, string XXX)
        {
            if (Typ == "IT" || Typ == "Fixed") { } else { return RedirectToAction("WhichTyp"); }
            if (AssetID == "") return RedirectToAction("MobileSet", new { Typ = Typ });
            return View("MobileSet", this.GetAssetViewModel(Typ, AssetID));
        }

        public ActionResult WhichTyp()
        {
            return View();
        }

        public ActionResult WhichGridTyp()
        {
            return View();
        }

        private AMSViewModel GetAssetViewModel(string Typ, string AssetID)
        {
            AMSViewModel vm = new AMSViewModel() { AmsMain = null };
            AMS_Main main = AssetRepo.Get(x => x.AssetID == AssetID);
            vm.Typ = Typ;
            if (main == null)
            {
                vm.ErrorMsg = "This ID was not matched in DB, would you like to remark it?";
                vm.OddAssetID = AssetID;
                return vm;
            }

            using (db = new AMSEntities())
            {
                var cc = db.vAMS_CC.SingleOrDefault();
                vm.CheckCC = cc.CheckedCC.Value;
                vm.TotalCC = cc.TotalCC.Value;
                vm.AmsMainlogs = db.AMS_MainLog
                                .Where(x => x.AssetID == AssetID)
                                .OrderByDescending(x => x.InsertDate).ToList();
                vm.Groups = GroupRepo.GetAll();
                vm.Locations = LocationRepo.GetAll();
                vm.Owner = OwnerRepo.GetAll();

                vm.AmsMain = main;
                var de=vm.Owner
                        .GroupBy(x => x.Owner.Substring(0, 1))
                        .Select(g => new { g.Key })
                        .OrderBy(x => x.Key).ToList();
                string ss="";
                foreach (var c in de) { ss += "<option value='" + c.Key + "'>" + c.Key + "</option>"; }
                ViewData["AZ"] = MvcHtmlString.Create(ss);
            };
            return vm;
        }

        [HttpPost]
        public ActionResult Save(string AssetID, string Owner,string Group,string Location, string Typ)
        {
            try
            {
                AMS_Main vm = AssetRepo.Get(x => x.AssetID == AssetID && x.Typ==Typ);
                if (Owner != "") { vm.EmpName = Owner; }
                if (Group != "") { vm.OwnerGroupID = Group; }
                if (Location != "") { vm.LocationID = Location; }
                AssetRepo.Update(vm);

                if (Owner != "") { SaveLog(AssetID, "EmpName", Owner); }
                if (Group != "") { SaveLog(AssetID, "OwnerGroupID", Group); }
                if (Location != "") { SaveLog(AssetID, "LocationID", Location); }
                return RedirectToAction("MobileQuery", "Asset", new { AssetID = AssetID, Typ=Typ });
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format(ErrMsg, "AssetID:" + AssetID, ex.ToString());
                logger.Debug(ErrMsg);
                return Json("Save err");
            }
        }

        [HttpPost]
        public ActionResult Mark(string AssetID, string Typ)
        {
            try
            {
                AMS_Main vm = AssetRepo.Get(x => x.AssetID == AssetID && Typ==Typ);
                vm.IsMarked = (vm.IsMarked == "Y") ? "N" : "Y";
                vm.MakedDate = DateTime.Now;
                AssetRepo.Update(vm);
                SaveLog(AssetID, "IsMarked", vm.IsMarked);
                return RedirectToAction("MobileQuery", "Asset", new { AssetID = AssetID, Typ=Typ });
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format(ErrMsg, "AssetID:" + AssetID, ex.ToString());
                logger.Debug(ErrMsg);
                return Json("Mark err");
            }
        }

        [HttpPost]
        public ActionResult CheckOK(string AssetID, string Typ) 
        {
            try
            {
                AMS_Main vm = AssetRepo.Get(x => x.AssetID == AssetID && x.Typ==Typ);
                //vm.IsChecked = "Y";
                vm.IsChecked = (vm.IsChecked == "Y") ? "N" : "Y";
                vm.CheckedDate = DateTime.Now;
                AssetRepo.Update(vm);
                SaveLog(AssetID, "IsChecked", vm.IsChecked);
                return RedirectToAction("MobileQuery", "Asset", new { AssetID = AssetID, Typ=Typ });
            }
            catch (Exception ex) 
            {
                ErrMsg=string.Format(ErrMsg, "AssetID:"+AssetID, ex.ToString());
                logger.Debug(ErrMsg);
                return Json("SaveCheckOK err");
            }
        }

       

        [HttpPost]
        public ActionResult RemarkOddID(string AssetID, string Typ) 
        {
            try
            {
                SaveLog(AssetID, "OddAssetID", AssetID);
                return RedirectToAction("MobileSet");
            }
            catch (Exception ex) 
            {
                ErrMsg=string.Format(ErrMsg, "AssetID:"+AssetID, ex.ToString());
                logger.Debug(ErrMsg);
                return Json("RemarkOddID err");
            }
        }

        public void SaveLog(string AssetID, string Field, string Value) { 
            AMS_MainLog log=new AMS_MainLog(){
                AssetID=AssetID,
                ColName=Field,
                After =Value,
                InsertDate=DateTime.Now,
                Updater=User.Identity.Name
            };

            try
            {
                LogRepo.Create(log);
            }
            catch (Exception ex) 
            {
                logger.Debug(ex.ToString());
            }
        }

        [HttpPost]
        [AllowCrossSiteJsonAttribute]
        public JsonResult PersonalAssetInfo(string u) 
        {
            string username=u;
            using (db = new AMSEntities())
            {
                var vm=db.v_AssetMain.Where(x => x.RealOwner.ToLower() == username.ToLower())
                    .OrderByDescending(x => x.Typ)        
                    .OrderByDescending(x=>x.MappingOwner)
                                .ToList();
                return Json(vm);
            }
        }


        /// Reference: http://kevintsengtw.blogspot.tw/2013/03/aspnet-mvc.html#.U6usmPmSwlQ
        [HttpPost]
        public ActionResult Upload(string Typ, HttpPostedFileBase file) {
            string msg = "OK";
            if (file == null) return Json("empty");

            if (file.ContentLength > 0) {
                try
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var DirPath = Server.MapPath("~/FileUploads" + Typ);
                    if (Directory.Exists(DirPath))
                    {
                        Directory.Delete(DirPath, true);
                    }

                    Directory.CreateDirectory(DirPath);
                    var path = Path.Combine(DirPath, fileName);
                    file.SaveAs(path);

                    //TryUploadExcelFile(Typ);
                }
                catch (Exception ex) {
                    msg = ex.Message.ToString();
                }
            }
            return RedirectToAction("Grid", new { Typ = Typ });
        }

        [HttpPost]
        public JsonResult TryUploadExcelFile(string Typ) {
                var DirPath=Server.MapPath("~/FileUploads"+Typ);
                string FilePath="";
                foreach(var d in Directory.EnumerateFiles(DirPath)){
                    //Console.WriteLine(d);
                    if (d != "") { FilePath = d; }
                }
                   
                //var excel = new LinqToExcel.ExcelQueryFactory(FilePath);
                //var dd = excel.Worksheet();
                //foreach (var c in dd) {
                //    Console.WriteLine(c);
                //}

                XSSFWorkbook excel;
                string 
                    AssetID = "", 
                    InventoryNo="",
                    AssetName="",
                    OwnerGroupID="",
                    StatusID="",
                    IsChecked="",
                    IsMarked="",
                    SubClass="",
                    LocationID="",
                    EmpName="",
                    Note="";
                DateTime? CheckedDate,MakedDate;

                using (db = new AMSEntities()) {
                    var es = db.AMS_Main.Where(x => x.Typ == Typ).ToList();
                    foreach (var e in es) {
                        e.IsEnable = "N";
                        e.UpdateTime = DateTime.Now;
                    }
                    db.SaveChanges();
                }

                using (FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read))
                {
                    excel = new XSSFWorkbook(fs);
                    NPOI.SS.UserModel.ISheet sheet = excel.GetSheetAt(0);
                    NPOI.SS.UserModel.IRow row;

                    

                    for (int k = 1; k <= sheet.LastRowNum; k++) // 使用For 走訪所有的資料列
                    {
                        if (sheet.GetRow(k) != null) // 驗證是不是空白列
                        {
                            row = sheet.GetRow(k);
                            AMS_Main ams;
                            if (Typ == "IT")
                            {
                                AssetID = row.GetCell(0).NumericCellValue.ToString();
                                InventoryNo = row.GetCell(1).StringCellValue;
                                AssetName = row.GetCell(3).StringCellValue;
                                OwnerGroupID = (row.GetCell(4) == null) ? "" : row.GetCell(4).StringCellValue;
                                StatusID = row.GetCell(5).StringCellValue;
                                EmpName = row.GetCell(6).StringCellValue;
                                IsChecked = (row.GetCell(7) == null) ? "" : row.GetCell(7).StringCellValue;
                                //CheckedDate = (row.GetCell(8) == null) ? null : row.GetCell(8).DateCellValue;
                                IsMarked = (row.GetCell(9) == null) ? "" : row.GetCell(9).StringCellValue;
                                //MakedDate = (row.GetCell(10) == null) ? null : row.GetCell(10).DateCellValue;
                                Note = (row.GetCell(11) == null) ? "" : row.GetCell(11).StringCellValue;

                                ams = new AMS_Main()
                                {
                                    AssetID = AssetID,
                                    InventoryNo = InventoryNo,
                                    AssetName = AssetName,
                                    OwnerGroupID = OwnerGroupID,
                                    StatusID = StatusID,
                                    EmpName = EmpName,
                                    IsChecked = IsChecked,
                                    //CheckedDate = CheckedDate,
                                    IsMarked = IsMarked,
                                    //MakedDate = MakedDate,
                                    Note = Note,
                                    Typ = Typ,
                                    UpdateTime = DateTime.Now
                                };
                                using (db = new AMSEntities())
                                {
                                    db.AddToAMS_Main(ams);
                                    db.SaveChanges();
                                }
                            }
                            else if (Typ=="Fixed") {
                                AssetID = row.GetCell(0).NumericCellValue.ToString();
                                InventoryNo = row.GetCell(1).StringCellValue;
                                SubClass = row.GetCell(2).StringCellValue;
                                AssetName = row.GetCell(3).StringCellValue.Replace("'","’");
                                LocationID = (row.GetCell(4)==null)?"":row.GetCell(4).StringCellValue;
                                EmpName = row.GetCell(6).StringCellValue;

                                ams = new AMS_Main()
                                {
                                    AssetID = AssetID,
                                    InventoryNo = InventoryNo,
                                    AssetName = AssetName,
                                    SubClass = SubClass,
                                    LocationID = LocationID,
                                    StatusID = StatusID,
                                    EmpName = EmpName,
                                    Typ = Typ,
                                    UpdateTime = DateTime.Now
                                };
                                 using (db = new AMSEntities())
                                 {
                                     db.AddToAMS_Main(ams);
                                     db.SaveChanges();
                                 }
                            }
                            
                        }
                    }

                }

                //string strConn = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + FilePath + ";" + "Extended Properties=\"Excel 8.0;IMEX=1\";";
                //OleDbConnection conn = new OleDbConnection(strConn);
                //conn.Open();
                //string strExcel = "";
                //OleDbDataAdapter myCommand = null;
                //DataSet ds = null;
                //strExcel = "select * from [sheet1$]";
                ////sheet1 是你的excel文件需要读取的那个sheet的名字
                //myCommand = new OleDbDataAdapter(strExcel, strConn);
                //ds = new DataSet();
                //myCommand.Fill(ds, "table1");
                //return ds;
                return Json("匯入成功! 請回到上一頁");
        }

        public ActionResult ExportExcelFile(string Typ) {

            if (Typ == "IT" || Typ == "Fixed") { } else {
                return RedirectToAction("Grid2");
            }

            using(db=new AMSEntities()){

                var vm = db.AMS_Main.Where(x => x.Typ == Typ)
                    .Select(g => new
                    {
                        AssetID = " "+g.AssetID,
                        g.InventoryNo,
                        g.SubClass,
                        g.AssetName,
                        g.OwnerGroupID,
                        g.StatusID,
                        g.EmpName,
                        g.IsChecked,
                        g.CheckedDate,
                        g.IsMarked,
                        g.MakedDate,
                        g.Note
                    })
                    .ToList();


                GridView gv = new GridView();
                gv.AllowPaging=false;   //關閉掉分頁與排序....
                gv.AllowSorting=false;
                gv.DataSource = vm;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                string DateStr = DateTime.Now.ToString("yyMMdd-Hmm");
                Response.AddHeader("content-disposition", "attachment;filename=AMS_Admin_" + DateStr + ".xls");
                Response.ContentType = "application/ms-excel";
                Response.ContentEncoding = System.Text.Encoding.Unicode;
                Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                //Response.Charset = "";

                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

                return RedirectToAction("Grid2");
            }
        }

        public JsonResult removeData(string Typ) {
            string msg = "";
            try
            {
                using (db = new AMSEntities())
                {
                    var es = db.AMS_Main.Where(x => x.Typ == Typ).ToList();
                    foreach (var e in es) {
                        db.DeleteObject(e);
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex) {
                msg = ex.Message.ToString();
            }
            return Json(msg);
        }
    }
}
