﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmailGroup.Models;
using EmailGroup.ViewModels;
using EmailGroup.Result;

namespace EmailGroup.Controllers
{
    public class QueryController : Controller
    {
        //
        // GET: /Query/
        GGEntities db;

        /// <summary>
        /// 由群組找尋有哪些email address
        /// </summary>
        /// <param name="GroupEmail"></param>
        /// <returns></returns>
        public ActionResult Index(string GroupEmail) {
            return View(QueryViewModelHandler(GroupEmail));
        }

        public ActionResult ReviewLog() {
            return View();
        }

        /// <summary>
        /// 20140606
        /// Do another page for ITS iframe only with a clear page and smaller font-size... 
        /// </summary>
        /// <param name="GroupEmail"></param>
        /// <returns></returns>
        public ActionResult ForITS(string GroupEmail) {
            return View("ForITS", QueryViewModelHandler(GroupEmail));
        }

        public ActionResult Tree(){
            return View();
        }

        public ActionResult TreeNg() {
            return View();
        }


        [HttpGet]
        public JsonResult Email(string emailaddress, int? emailSN) { 
            using (db=new GGEntities()){
                var vm = db.v_MailTree
                    .Select(g => new { c = g.Child
                                    , pp = g.Parent
                                    , lv = g.LEVEL
                                    , sort=g.ID
                                    , sn=g.SN
                                    , isVisible=g.IsUVisible
                                    , sortname=g.Sort
                                    , AD=g.AD
                                    , GG = g.GG
                    }).AsQueryable();
                if(emailaddress!=null){
                    vm=vm.Where(x=>x.c==emailaddress);
                }

                if (emailSN!=null && emailSN.Value > 0) {
                    vm=vm.Where(x => x.sn == emailSN);
                }
                
                return Json(vm.ToList(), JsonRequestBehavior.AllowGet);
            }
            
        }

        /// <summary>
        /// Find the corrosponing group name.
        /// </summary>
        /// <param name="pp_email"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult EmailGG(string email) {
            using (db = new GGEntities()) {
                var vm = db.EmailGroup_Mapping.Where(x => x.GmailAddress == email).ToList();
                return Json(vm.ToList(), JsonRequestBehavior.AllowGet);
            }
        }
        

        [HttpPost]
        public JsonResult EmailDetails(string emailAddress) {
            string email = emailAddress;
            using (db = new GGEntities()) {
                var vm = db.EmailGroup_Emails.Where(x => x.GroupEmail == emailAddress).ToList();
                return Json(vm);
            }
        }

        [HttpPost]
        public void SaveEmail(EmailDetail EmailDetail) {           
            bool IsNewNode = false;
            using (db = new GGEntities()) {
                IsNewNode = (EmailDetail.sn == 0) ? true : false;
                // AD,GG必有值才異動。
                if(string.IsNullOrEmpty(EmailDetail.AD) || string.IsNullOrEmpty(EmailDetail.GG)){

                } else {
                    if (IsNewNode) {
                        try {
                            CreateLog("Add Group", EmailDetail);
                            EmailGroup_Mapping e = new EmailGroup_Mapping() {
                                ADGroup = EmailDetail.AD,
                                GmailGroup = EmailDetail.GG,
                                GmailAddress = EmailDetail.c,
                                IsUserVisible = EmailDetail.IsVisible,
                                Updatetime = DateTime.Now
                            };
                            db.EmailGroup_Mapping.AddObject(e);
                            db.SaveChanges();
                        } catch (Exception ex) {
                            CreateLog("Add Group", EmailDetail, ex);
                        }
                    } else {
                        try {
                            CreateLog("Update Group", EmailDetail);
                            int sn = EmailDetail.sn;
                            string Gmail = db.EmailGroup_Emails
                                .Where(x => x.SN == sn).SingleOrDefault().GmailAddress.Trim();
                            EmailGroup_Mapping m = db.EmailGroup_Mapping
                                .Where(x => x.GmailAddress == Gmail).SingleOrDefault();
                            m.ADGroup = EmailDetail.AD;
                            m.GmailGroup = EmailDetail.GG;
                            m.Updatetime = DateTime.Now;
                            m.IsUserVisible = EmailDetail.IsVisible;
                            db.SaveChanges();
                        } catch (Exception ex) {
                            CreateLog("Update Group", EmailDetail, ex);
                        }
                    }

                    try {
                        CreateLog("Loop Visible", EmailDetail);
                        //--將該節點以下的都全部隱藏or顯示---
                        var SNs = db.v_MailTree
                            .Where(x => x.Sort.Contains(EmailDetail.sortname + "/"))
                            .Select(g => new { TargetSN = g.SN }).ToList();
                        foreach (var sn in SNs) {
                            EmailGroup_Emails e = db.EmailGroup_Emails
                                .Where(x => x.SN == sn.TargetSN).SingleOrDefault();
                            e.IsUserVisible = EmailDetail.IsVisible;
                            e.UpdateTime = DateTime.Now;
                            db.SaveChanges();
                        }
                    } catch (Exception ex) {
                        CreateLog("Loop Visible", EmailDetail, ex);
                    }
                    
                }

                if (IsNewNode) {
                    // 新增
                    try {
                        CreateLog("Create Email", EmailDetail);
                        EmailGroup_Emails e = new EmailGroup_Emails() {
                            GroupEmail = EmailDetail.pp,
                            GmailAddress = EmailDetail.c,
                            IsUserVisible = EmailDetail.IsVisible,
                            UpdateTime = DateTime.Now
                        };
                        db.EmailGroup_Emails.AddObject(e);
                        db.SaveChanges();
                    } catch (Exception ex) {
                        CreateLog("Create Email", EmailDetail, ex);
                    }
                } else {
                    // 修改
                    try {
                        CreateLog("Update Email", EmailDetail);
                        var e = db.EmailGroup_Emails.Where(x => x.SN == EmailDetail.sn).SingleOrDefault();
                        e.GmailAddress = EmailDetail.c;
                        e.GroupEmail = EmailDetail.pp;
                        e.UpdateTime = DateTime.Now;
                        e.IsUserVisible = EmailDetail.IsVisible;
                        db.SaveChanges();
                    } catch (Exception ex) {
                        CreateLog("Update Email", EmailDetail, ex);
                    }
                }


            }

        }

        /// <summary>
        /// 刪除Email....
        /// </summary>
        /// <param name="EmailDetail"></param>
        [HttpPost]
        public void DeleteEmail(EmailDetail EmailDetail) {
            using (db = new GGEntities()) {
                if (EmailDetail.sn >0) {
                    // 新增
                    try {
                        EmailGroup_Emails e = db.EmailGroup_Emails
                            .Where(x => x.SN == EmailDetail.sn).SingleOrDefault();
                        db.DeleteObject(e);
                        db.SaveChanges();
                        CreateLog("Delete Email", EmailDetail);
                    } catch (Exception ex) {
                        //If error, write to DB....
                        CreateLog("Delete Email", EmailDetail, ex);
                    }
                }
            }
        }

        private void CreateLog(string ActionName, EmailDetail emailDetail) {
            EmailGroup_Log log = new EmailGroup_Log() {
                AD = emailDetail.AD,
                C = emailDetail.c,
                GG = emailDetail.GG,
                PP = emailDetail.pp,
                OriginSN = emailDetail.sn,
                IsVisible = emailDetail.IsVisible,
                InsertDate = DateTime.Now,
                UpdateUser = User.Identity.Name,
                ActionName = ActionName
            };
            db.EmailGroup_Log.AddObject(log);
            db.SaveChanges();
        }

        private void CreateLog(string ActionName, EmailDetail emailDetail, Exception ex) {
            EmailGroup_Log log = new EmailGroup_Log() {
                AD = emailDetail.AD,
                C = emailDetail.c,
                GG = emailDetail.GG,
                PP = emailDetail.pp,
                OriginSN = emailDetail.sn,
                InsertDate = DateTime.Now,
                UpdateUser = User.Identity.Name,
                ActionName = ActionName,
                Errmsg = ex.ToString(),
                Typ="Error"
            };
            db.EmailGroup_Log.AddObject(log);
            db.SaveChanges();
        }

        [HttpPost]
        public JsonResult QueryGroupEmail(string GroupEmail) {
            return Json(QueryViewModelHandler(GroupEmail));
        }

        [HttpPost]
        public JsonNetResult ShowLogJson() {
            using (db = new GGEntities()) {
                var vm = db.EmailGroup_Log.OrderByDescending(x => x.sn).Take(100);
                return new JsonNetResult() { Data = vm.ToList() };
            }
        }

        private QueryViewModel QueryViewModelHandler(string GroupEmail) {
            using (db = new GGEntities()) {
                var mm = db.EmailGroup_Emails
                    .Where(x => x.GroupEmail == GroupEmail && x.GmailAddress != "" && x.IsUserVisible!="N")
                    .AsQueryable();

                QueryViewModel vm = new QueryViewModel() {
                    EmailGroup_Mappings = db.EmailGroup_Mapping
                        .Where(x=>x.IsUserVisible=="Y")
                        .OrderBy(x => x.ADGroup).ToList(),
                    Prefix = mm.GroupBy(x => x.GmailAddress.Substring(0, 1)).Select(g => g.Key).ToList(),
                    TotalEmailCC = mm.ToList().Count,
                    SelectedGroupEmail = GroupEmail
                };

                if (vm.TotalEmailCC > 0) {
                    vm.EmailGroup_Emails = mm.ToList();
                }

                return vm;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="returnUrl">For routing parameters e.g.: Index|ForITS </param>
        /// <returns></returns>
        public ActionResult FindGroup(string email, string returnUrl) {
            using (db = new GGEntities()) {
                QueryViewModel vm = new QueryViewModel() {
                    EmailGroup_Mappings = db.EmailGroup_Mapping.OrderBy(x => x.ADGroup).ToList()
                };
                var mm=db.v_EmailGroup.Where(x => x.Email == email).AsQueryable();
                vm.v_EmailGroups = mm.ToList();
                vm.TotalGroupCC = mm.Count();
                vm.QueridEmail = email;
                return View(returnUrl, vm);
            }
        }


        [HttpPost]
        public JsonResult SuggestEmail(string sg) {
            int iLen = 0;
            string v = sg.Trim();
            if (string.IsNullOrEmpty(v)) {
                return Json("");
            } else {
                iLen = sg.Length;
                using (db = new GGEntities()) {
                    var vm = db.EmailGroup_Emails
                                .Where(x => x.GmailAddress.Substring(0, iLen) == v)
                                .GroupBy(x=>x.GmailAddress)
                                .Select(g=>new { email= g.Key })
                                .ToList();
                    return Json(vm);
                }
            }
        }

      



    }
}
