﻿'use strict';
module.exports = function (grunt) {
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.initConfig({
    watch: {
      coffeefiles: {
        files: ['app/assets/javascripts/*.js'],
        options: {
          livereload: true,
        }
      },
      erbfiles: {
        files: ['app/views/{,*/}*.erb'],
        options: {
          livereload: true,
        }
      },
      cshtml:{
        files: ['Views/{,*/}*.cshtml'],
        options: {
          livereload: true,
        }
      }
    }
  });
}