'use strict';

/* Controllers */

var emailControllers = angular.module('emailControllers', []);
var root = {
    "c": "root@onelab.tw",
    "pp": "root",
    "lv": 0,
    "sort": 1,
    "sn": 9999,
    "sortname": "aaa"
};

emailControllers.controller("EmailTreeCtrl", ["$scope", "EmailServ", "$q"
    , function ($scope, EmailServ, $q) {
        EmailServ.query(function (data) {
            $scope.emails = data;
            $scope.emails.push(root);
            $scope.ReviewLog();
            //console.log($scope.emails, $scope.emails.length);
        });
        //        $scope.EmailDetail = {};
        //        $scope.EmailGGAD = {};

        //click HideShow.
        $scope.HideShow = function (e) {
            var srt = e.sortname + "/";
            angular.forEach($scope.emails, function (email) {
                if (email.sortname.indexOf(srt) === 0) {
                    email.myClass = (e.isHide) ? "ng-hide" : "";
                }
            });
            e.isHide = (e.isHide === true) ? false : true;
        }
        //click ShowDetails.
        $scope.ShowDetails = function (sn, email) {

            EmailServ.get({ emailSN: sn }, function (data) {
                //console.log(data);
                $scope.EmailDetail = data[0];
            }, function (error) {
                //console.error(error);
            });

            //Get the AD,GG. From other table datas...
            EmailServ.getGroup({ email: email }, function (data) {
                //console.log(data);
                $scope.EmailGGAD = data[0];
            }, function (error) {
                //console.error(error);
            });
        }

        $scope.SaveEmail = function (EmailDetail, EmailGGAD) {
            //console.log("before SAVA...", EmailDetail, EmailGGAD);
            $scope.EmailDetail["AD"] = (EmailGGAD != undefined) ? EmailGGAD.ADGroup : "";
            $scope.EmailDetail["GG"] = (EmailGGAD != undefined) ? EmailGGAD.GmailGroup : "";

            EmailServ.save($scope.EmailDetail, function (data) {
                //console.log("saved...", data);
                EmailServ.query(function (data) {
                    $scope.emails = data;
                    $scope.emails.push(root);
                });
            }, function (error) {
                //console.log(error);
            });

        }

        $scope.CreatEmailNode = function () {
            $scope.EmailDetail = {};
            $scope.EmailGGAD = {};
        }

        $scope.CheckEmailNode = function (EmailGGAD) {
            $scope.EmailDetail["AD"] = (EmailGGAD != undefined) ? EmailGGAD.ADGroup : "";
            $scope.EmailDetail["GG"] = (EmailGGAD != undefined) ? EmailGGAD.GmailGroup : "";
            //console.log($scope.EmailDetail);
        }

        $scope.DeleteEmail = function (EmailDetail) {
            EmailServ.deleteMail($scope.EmailDetail
            , function (data) {
                EmailServ.query(function (data) {
                    $scope.emails = data;
                    $scope.emails.push(root);
                });
            }, function (error) {
                //console.log(error);
            });
        }

        $scope.ReviewLog = function () {
            EmailServ.showLog(null, function (data) {
                $scope.logs = data;
            }, function (err) { });
        }


    }
  ]);
