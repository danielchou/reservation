﻿'use strict';

/* App Module */

var email_app = angular.module('email_app', [
  "emailControllers",
  "emailServices"
  ]);