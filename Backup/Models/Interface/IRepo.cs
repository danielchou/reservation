﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AMS2.Models.Interface
{

    public interface IRepo<TEntity> : IDisposable
    where TEntity : class
    {
        void Create(TEntity instance);
        void Update(TEntity instance);
        string Edit(TEntity instance);
        void Delete(TEntity instance);

        /// <summary>
        /// 該筆資料是否已經存在?
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        bool IsAddNew(Expression<Func<TEntity, bool>> predicate);

        int? MaxSN(Expression<Func<TEntity, int?>> predicate);

        TEntity Get(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);

        void SaveChanges();

    }
    
}
