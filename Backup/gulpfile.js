var gulp =require("gulp"),
    connect = require("gulp-connect"),
    cssmin = require('gulp-cssmin'),
    $ = require("gulp-load-plugins")();  /* https://www.npmjs.com/package/gulp-load-plugins */

gulp.task("js-min",function(){
    return gulp.src([
        "bower_components/jquery/dist/jquery.js",
        "bower_components/semantic-ui/dist/semantic.js",
        "bower_components/moment/moment.js",
        "bower_components/fullcalendar/dist/fullcalendar.js",
        "bower_components/vue/dist/vue.js",
    ])
    .pipe($.jshint())
    .pipe($.jshint.reporter("default"))
    .pipe($.uglify())
    .pipe($.concat("m5.js"))
    .pipe(gulp.dest("Content/dist"));
});

/****************************************************************************/
var foldersToMove = [
    "bower_components/bootstrap/dist/**/*.*",
    "bower_components/fullcalendar/dist/*.*",
    "bower_components/jquery/dist/*.*",
    "bower_components/jqueryui/themes/pepper-grinder/*.*",
    "bower_components/jqueryui/ui/minified/*.*",
    "bower_components/jqueryui/jquery-ui.min.js",
    "bower_components/requirejs/require.js",
    "bower_components/semantic-ui/dist/**/*.*",
    "bower_components/vue/dist/*.*",
    "bower_components/validator/*.*",
    "bower_components/vue-validator/dist/*.*",
];
gulp.task("move_bower_dist", function () {
    return gulp.src(foldersToMove, { base: "./bower_components/" })
        .pipe(gulp.dest("Scripts/vendor"));
});

gulp.task("js-layout-dev", function () {
    return gulp.src([
        "bower_components/jquery/dist/jquery.js",
        "bower_components/bootstrap/dist/js/bootstrap.js",
        "bower_components/vue/dist/vue.js",
        "bower_components/vue-validator/dist/vue-validator.js",
        "Scripts/myPlugin.js"
    ])
    .pipe($.concat("layout-dev.js"))
    .pipe(gulp.dest("Content/dist"));
});

//啟用WebServer.
gulp.task("connect", function(){
    connect.server({
        root:"",
        livereload:true
    });
});

gulp.task("html", function(){
    gulp.src("./17Y/*.*")
        .pipe(connect.reload());
});

var minifyHTML = require('gulp-minify-html');
gulp.task('minify-html-showroom', function () {
    var opts = {
        conditionals: true,
        spare: true
    };

    return gulp.src('Content/views/showroom/tmp.html')
        .pipe(minifyHTML(opts))
        .pipe($.concat("tmp.min.html"))
        .pipe(gulp.dest("Content/views/showroom"));
        //.pipe(gulp.dest("Content/views/showroom-tmp.html"))
});


gulp.task('css-min', function () {
    gulp.src([
            "bower_components/semantic-ui/dist/semantic.css",
            "bower_components/fullcalendar/dist/fullcalendar.css"
        ])
    .pipe(cssmin())
    .pipe($.concat("main.min.css"))
    .pipe(gulp.dest('Content/dist'));
});


gulp.task("min-ams-js", function () {
    return gulp.src([
        "Scripts/vendor/jquery/dist/jquery.min.js",
        "Scripts/vendor/bootstrap/dist/js/bootstrap.min.js",
        "Scripts/vendor/vue/dist/vue.min.js"
    ])
    .pipe($.uglify())
    .pipe($.concat("min.js"))
    .pipe(gulp.dest("Scripts/src"));
});

//gulp.task("default",["connect","watch"]);
//gulp.task("default", ["js-layout", "css-min", "minify-html-showroom", "js-showroom"]);
gulp.task("minjs", ["min-ams-js"]);
gulp.task("vendor", ["move_bower_dist"]);