﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmailGroup.Models;

namespace EmailGroup.ViewModels
{
    public class QueryViewModel
    {
        public IEnumerable<EmailGroup_Mapping> EmailGroup_Mappings { get; set; }
        public IEnumerable<EmailGroup_Emails> EmailGroup_Emails { get; set; }
        public IEnumerable<v_EmailGroup> v_EmailGroups { get; set; }
        public int TotalEmailCC { get; set; }
        public int TotalGroupCC { get; set; }
        public string SelectedGroupEmail { get; set; }
        public string QueridEmail { get; set; } 
        public IList<string> Prefix { get; set; }
    }

    public class EmailDetail {
        public int sn { get; set; }
        public string pp { get; set; }
        public string c { get; set; }
        public string AD { get; set; }
        public string GG { get; set; }
        public string IsVisible { get; set; }
        public string sortname { get; set; }
    }
}