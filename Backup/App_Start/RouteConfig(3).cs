﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MicroFlowSideProj {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
              name: "MailBody", url: "{controller}/{action}/{mGuid}",
              defaults: new { controller = "Watch", action = "MailBody", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default", url: "{controller}/{action}/{id}",
                defaults: new { controller = "Watch", action = "Main", id = UrlParameter.Optional }
            );
        }
    }
}