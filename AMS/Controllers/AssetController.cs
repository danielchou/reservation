﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AMS.Models;
using AMS.Models.Interface;
using AMS.ViewModels;
using NLog;
using AMS.Attr;
using System.Collections;

namespace AMS.Controllers
{
    public class AssetController : Controller
    {
        AMSEntities db;
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IRepo<AMS_Main> AssetRepo;
        private IRepo<AMS_MainLog> LogRepo;
        private IRepo<vAMS_Owner> OwnerRepo;
        private IRepo<AMS_OwnerGroup> GroupRepo;
        private IRepo<AMS_Location> LocationRepo;
        private string ErrMsg = "{0}\n{1}";

        public AssetController() 
        {
            this.AssetRepo = new GenericRepo<AMS_Main>();
            this.LogRepo = new GenericRepo<AMS_MainLog>();
            this.OwnerRepo = new GenericRepo<vAMS_Owner>();
            this.GroupRepo = new GenericRepo<AMS_OwnerGroup>();
            this.LocationRepo = new GenericRepo<AMS_Location>();
            
        }

        //這是原本.net MVC舊版本的部分...
        public ActionResult oldGrid(string fn, string sort)
        {

            var vm = AssetRepo.GetAll();
            if (fn == "EmpName" && sort == "desc") { vm = vm.OrderByDescending(x => x.EmpName); }
            if (fn == "EmpName" && sort == "asc") { vm = vm.OrderBy(x => x.EmpName); }

            if (fn == "OwnerGroupID" && sort == "desc") { vm = vm.OrderByDescending(x => x.OwnerGroupID); }
            if (fn == "OwnerGroupID" && sort == "asc") { vm = vm.OrderBy(x => x.OwnerGroupID); }

            if (fn == "IsCheck" && sort == "desc") { vm = vm.OrderByDescending(x => x.IsChecked).ThenByDescending(x => x.EmpName); }
            if (fn == "IsCheck" && sort == "asc") { vm = vm.OrderBy(x => x.IsChecked).ThenBy(x => x.EmpName); }

            if (fn == "IsMark" && sort == "desc") { vm = vm.OrderByDescending(x => x.IsMarked).ThenByDescending(x => x.EmpName); }
            if (fn == "IsMark" && sort == "asc") { vm = vm.OrderBy(x => x.IsMarked).ThenBy(x => x.EmpName); }

            if (fn == "AssetID" && sort == "desc") { vm = vm.OrderByDescending(x => x.AssetID); }
            if (fn == "AssetID" && sort == "asc") { vm = vm.OrderBy(x => x.AssetID); }

            if (fn == "AssetName" && sort == "desc") { vm = vm.OrderByDescending(x => x.AssetName); }
            if (fn == "AssetName" && sort == "asc") { vm = vm.OrderBy(x => x.AssetName); }

            return View(vm.ToList());
        }

        //什麼都不做，只吐出乾淨的頁面!!
        public ActionResult Grid()
        {
            return View("Grid2"); //Grid2.cshtml
        }


        /// <summary>
        /// 提供給Grid的頁面的ajax資料來源
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GridAjax()
        {
            var vm = AssetRepo.GetAll();
            return Json(vm);
        }

        [HttpPost]
        public JsonResult SaveAsset(AMS_Main asset) {
            using (db = new AMSEntities())
            {
                string msg = "Done.";
                try
                {
                    var e = db.AMS_Main.Where(x => x.AssetID == asset.AssetID).SingleOrDefault();
                    if (e != null)
                    {
                        e.Note = asset.Note;
                        e.UpdateTime = DateTime.Now;
                        db.SaveChanges();
                    }
                    else {
                        msg = "Can't find this asset.";
                    }
                }
                catch (Exception ex) {
                    msg=ex.Message.ToString();
                }
                return Json(msg);
            }
        }

        public ActionResult MobileSet() 
        {
            return View();
        }

        [HttpPost]
        public ActionResult MobileQuery(string AssetID)
        {
            if (AssetID == "") return RedirectToAction("MobileSet");
            return View("MobileSet", this.GetAssetViewModel(AssetID));
        }

        [HttpGet]
        public ActionResult MobileQuery(string AssetID, string XXX)
        {
            if (AssetID == "") return RedirectToAction("MobileSet");

            return View("MobileSet", this.GetAssetViewModel(AssetID));
        }

        private AMSViewModel GetAssetViewModel(string AssetID)
        {
            AMSViewModel vm = new AMSViewModel() { AmsMain = null };
            AMS_Main main = AssetRepo.Get(x => x.AssetID == AssetID);
            if (main == null)
            {
                vm.ErrorMsg = "This ID was not matched in DB, would you like to remark it?";
                vm.OddAssetID = AssetID;
                return vm;
            }

            using (db = new AMSEntities())
            {
                var cc = db.vAMS_CC.SingleOrDefault();
                vm.CheckCC = cc.CheckedCC.Value;
                vm.TotalCC = cc.TotalCC.Value;
                vm.AmsMainlogs = db.AMS_MainLog
                                .Where(x => x.AssetID == AssetID)
                                .OrderByDescending(x => x.InsertDate).ToList();
                vm.Groups = GroupRepo.GetAll();
                vm.Locations = LocationRepo.GetAll();
                vm.Owner = OwnerRepo.GetAll();
               
                vm.AmsMain = main;
                var de=vm.Owner
                        .GroupBy(x => x.Owner.Substring(0, 1))
                        .Select(g => new { g.Key })
                        .OrderBy(x => x.Key).ToList();
                string ss="";
                foreach (var c in de) { ss += "<option value='" + c.Key + "'>" + c.Key + "</option>"; }
                ViewData["AZ"] = MvcHtmlString.Create(ss);
            };
            return vm;
        }

        [HttpPost]
        public ActionResult Save(string AssetID, string Owner,string Group,string Location)
        {
            try
            {
                AMS_Main vm = AssetRepo.Get(x => x.AssetID == AssetID);
                if (Owner != "") { vm.EmpName = Owner; }
                if (Group != "") { vm.OwnerGroupID = Group; }
                if (Location != "") { vm.LocationID = Location; }
                AssetRepo.Update(vm);

                if (Owner != "") { SaveLog(AssetID, "EmpName", Owner); }
                if (Group != "") { SaveLog(AssetID, "OwnerGroupID", Group); }
                if (Location != "") { SaveLog(AssetID, "LocationID", Location); }
                return RedirectToAction("MobileQuery", "Asset", new { AssetID = AssetID });
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format(ErrMsg, "AssetID:" + AssetID, ex.ToString());
                logger.Debug(ErrMsg);
                return Json("Save err");
            }
        }

        [HttpPost]
        public ActionResult Mark(string AssetID)
        {
            try
            {
                AMS_Main vm = AssetRepo.Get(x => x.AssetID == AssetID);
                vm.IsMarked = (vm.IsMarked == "Y") ? "N" : "Y";
                vm.MakedDate = DateTime.Now;
                AssetRepo.Update(vm);
                SaveLog(AssetID, "IsMarked", vm.IsMarked);
                return RedirectToAction("MobileQuery", "Asset", new { AssetID = AssetID });
            }
            catch (Exception ex)
            {
                ErrMsg = string.Format(ErrMsg, "AssetID:" + AssetID, ex.ToString());
                logger.Debug(ErrMsg);
                return Json("Mark err");
            }
        }

        [HttpPost]
        public ActionResult CheckOK(string AssetID) 
        {
            try
            {
                AMS_Main vm = AssetRepo.Get(x => x.AssetID == AssetID);
                vm.IsChecked = "Y";
                vm.CheckedDate = DateTime.Now;
                AssetRepo.Update(vm);
                SaveLog(AssetID, "IsChecked", "Y");
                return RedirectToAction("MobileQuery", "Asset", new { AssetID = AssetID });
            }
            catch (Exception ex) 
            {
                ErrMsg=string.Format(ErrMsg, "AssetID:"+AssetID, ex.ToString());
                logger.Debug(ErrMsg);
                return Json("SaveCheckOK err");
            }
        }

       

        [HttpPost]
        public ActionResult RemarkOddID(string AssetID) 
        {
            try
            {
                SaveLog(AssetID, "OddAssetID", AssetID);
                return RedirectToAction("MobileSet");
            }
            catch (Exception ex) 
            {
                ErrMsg=string.Format(ErrMsg, "AssetID:"+AssetID, ex.ToString());
                logger.Debug(ErrMsg);
                return Json("RemarkOddID err");
            }
        }

        public void SaveLog(string AssetID, string Field, string Value) { 
            AMS_MainLog log=new AMS_MainLog(){
                AssetID=AssetID,
                ColName=Field,
                After =Value,
                InsertDate=DateTime.Now,
                Updater=User.Identity.Name
            };

            try
            {
                LogRepo.Create(log);
            }
            catch (Exception ex) 
            {
                logger.Debug(ex.ToString());
            }
        }

        [HttpPost]
        [AllowCrossSiteJsonAttribute]
        public JsonResult PersonalAssetInfo(string u) 
        {
            string username=u;
            using (db = new AMSEntities())
            {
                var vm=db.v_AssetMain.Where(x => x.RealOwner == username)
                                .OrderByDescending(x=>x.MappingOwner).ToList();
                return Json(vm);
            }
        }

    }
}
