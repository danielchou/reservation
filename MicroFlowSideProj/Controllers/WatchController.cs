﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MicroFlowSideProj.Models;

namespace MicroFlowSideProj.Controllers
{
    public class WatchController : Controller
    {
        //
        // GET: /Watch/
        EformsystemEntities db;

        public ActionResult Main() {
            using (db = new EformsystemEntities()) {
                var vm = db.A_SignCRRForm
                    .OrderByDescending(x => x.ReqDate)
                    .ToList();
                return View(vm);
            }
        }

        public ActionResult MailList(string SerialNumber) {
            if (string.IsNullOrEmpty(SerialNumber)) {
                return View("");
            } else {
                using (db = new EformsystemEntities()) {
                    var vm = db.v_MailList
                        .Where(x => x.FormSN == SerialNumber)
                        .OrderByDescending(x => x.CreateTime)
                        .ToList();
                    return View(vm);
                }
            }
        }

        public ActionResult MailBody(string mGuid) {
            if (mGuid != null) {
                using (db = new EformsystemEntities()) {
                    var email = db.A_MailList
                                    .Where(x => x.mGUID == mGuid)
                                    .SingleOrDefault();
                    return View(email);
                }
            } else {
                return View();
            }
        }

    }
}



