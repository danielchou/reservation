﻿using System;
using System.Data.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reservation.Models;
using Reservation.Attritube;
using Reservation.ViewModels;
using CommonLib.Interface;
using CommonLib.Repository;
using System.Data.SqlClient;
using Microsoft.Security.Application;

namespace Reservation.Controllers
{
    public class MeetingRoomController : Controller
    {
        //
        // GET: /Reserve/
        BookingEntities db;
        private IRepo<Reserve> RepoReserve;

        public MeetingRoomController() {
            this.RepoReserve = new GenericRepo<Reserve>();
        }

        [AllowCrossSiteJsonAttribute]
        public ActionResult Main(Reserve e)
        {
            using (db = new BookingEntities()) {
                
                ReservViewModel vm = new ReservViewModel() {
                    Reserves = e,
                    Rooms = db.Rooms.Where(x=>x.IsDisable!="Y").ToList(),
                    RoomDisabled = db.Rooms.Where(x=>x.IsDisable=="Y").ToList(),
                };
                return View(vm);
            }
        }

        [AllowCrossSiteJsonAttribute]
        public ActionResult Main2Tab(Reserve e, string DefaultRoom)
        {
            using (db = new BookingEntities())
            {
                try
                {
                    ReservViewModel vm = new ReservViewModel()
                    {
                        DefaultRoom = DefaultRoom,
                        Reserves = e,
                        Rooms = db.Rooms.Where(x => x.IsDisable != "Y").ToList(),
                        RoomDisabled = db.Rooms.Where(x => x.IsDisable == "Y").ToList(),
                    };
                    return View("Main", vm);
                }
                catch (Exception ex) {
                    return RedirectToAction("ErrorPage", ex);
                }
            }
        }


        public ActionResult ErrorPage(Exception ee) {
            return View(ee);
        }
        
        /// <summary>
        /// Old design not safe...
        /// </summary>
        /// <param name="email"></param>
        /// <param name="gid"></param>
        /// <returns></returns>
        [AllowCrossSiteJsonAttribute]
        public ActionResult TransferAccount(string email,string gid) 
        {
            string user = "",errmsg="";
            string[] arr = email.Split(new string[]{"@"}, StringSplitOptions.None);
            if (arr.Length >= 1)
            {
                user = arr[0];
            }
            else {
                errmsg = "帳號有錯誤，請從ITS進入，謝謝!";
            }
            LoginInfor e = new LoginInfor
            {
                Email = email,
                GID = gid,
                UserName = user,
                ErrMsg = errmsg
            };
            return View(e);
        }

        /// <summary>
        /// New design 2015.
        /// </summary>
        /// <param name="UserAppToken"></param>
        /// <param name="Room"></param>
        /// <returns></returns>
        [AllowCrossSiteJsonAttribute]
        public ActionResult TransferAccountToBooking(string UserAppToken, string Room)
        {
            string Email = "", gid = "", UserName = "", errmsg = "";
            using (OLPEntities db = new OLPEntities()) {
                var u = db.vOneUserRoles.Where(x => x.GID == UserAppToken).FirstOrDefault();
                if (u != null) {
                    Email = u.email;
                    UserName = u.username;
                }
            }
            Room = (Room.Length > 4) ? Room.Replace("room", "") : Room; //洗資料, roomA2==>A2
            LoginInfor e = new LoginInfor
            {
                Email = Email,
                UserName = UserName,
                DefaultRoom=Room,
                FromSite="OLP",

                GID = gid,
                ErrMsg = errmsg
            };
            return View("TransferAccount", e);
        }

        //尚未完成
        [AllowCrossSiteJsonAttribute]
        public JsonResult GetMyBookingThisWeek(string UserName)
        {
            string Today = DateTime.Today.Date.ToShortDateString();
            string NextWk = DateTime.Today.AddDays(7.0).ToShortDateString();
            var model = db.Reserves.Where(x=>x.Creator == UserName).ToList();
            return Json(model);
        }

        /// <summary>
        /// 月曆預約會議室資料從這邊打出來
        /// </summary>
        /// <param name="roomid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ShowReservation(string roomid)
        {
            string roomid2 = "F" + roomid;
            using (db = new BookingEntities()) 
            {
                var model = (roomid == "All")
                    ? db.vReserves.Where(x=>x.title!=null).ToList()
                    : db.vReserves.Where(x => x.className == roomid2).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetOneReservation(string guid)
        {
            Guid gid = new Guid(guid);
            using (db = new BookingEntities())
            {
                var reserv = db.Reserves.Where(x => x.GUID == gid).SingleOrDefault();
                var repeat = db.RepeatAgendas.Where(x => x.RepeatID == reserv.RepeatID).SingleOrDefault();
                if (reserv.IsRepeat != "Y") { 
                    repeat = null;
                    reserv.RepeatID = null;
                }    
                return Json(new { Reserve = reserv, Repeat = repeat }, JsonRequestBehavior.AllowGet);
            }
        }

         #region  ee
        [HttpPost]
        public ActionResult Save(Reserve e)
        {
            //Sanitizer....
            //e.Note = Sanitizer.GetSafeHtml(e.Note);
            e.Agenda = Encoder.HtmlEncode(e.Agenda);
            e.Note = Encoder.HtmlEncode(e.Note);

            try {
                if (e.IsRepeat == "true" || e.IsRepeat == "on") {
                    string RepeatErrDates = IsExistReservedOnDates(e);
                    if (RepeatErrDates != "") {
                        if (e.RepeatAgenda != null) {
                            e.RA_RptOnWkDay = e.RepeatAgenda.RptOnWkDay;
                            e.RA_RptStartDate = e.RepeatAgenda.RptStartDate;
                            e.RA_RptEvery = e.RepeatAgenda.RptEvery;
                            e.RA_RptEndTyp = e.RepeatAgenda.RptEndTyp;
                            e.RA_RptEndAfterOccur = e.RepeatAgenda.RptEndAfterOccur;
                            e.RA_RptEndDay = e.RepeatAgenda.RptEndDay;
                        }
                        string[] RepeatErrDatesArr = RepeatErrDates.Split(new string[] { "@" }, StringSplitOptions.RemoveEmptyEntries);
                        int icount = RepeatErrDatesArr.Length - 1;
                        string DupDates = "";
                        //DupDates += "共有" + icount.ToString() + "天的日期已被預約。@";
                        DupDates += RepeatErrDates + "@";
                        e.ErrorMsg = DupDates + " 被預約了!";
                        return RedirectToAction("Main", e);
                    }
                    RepeatSave(e);
                } else {
                    if (e.SDateTime.ToString().CompareTo(e.EDateTime) >= 0) {
                        e.ErrorMsg = "請檢查您的預約時間。";
                        return RedirectToAction("Main", e);
                    }

                    if (e.GUID == null) {
                        bool IsResereDup = IsReserveExisted(e.Room, e.SDateTime, e.EDateTime, e.Creator);
                        if (IsResereDup) {
                            e.ErrorMsg = e.StartDateTime + " 該時間區段上有人預約了!";
                            return RedirectToAction("Main", e);
                        }
                    }

                    SaveSingleReserve(e);
                }
            } catch (Exception ex) {
                e.ErrorMsg = ex.ToString();
            }
            return RedirectToAction("Main",e);
        }


        public bool IsReserveExisted(string Room, string StartDateTime, string EndDateTime , string Creator) {
            //string _Creator="";
            using (db = new BookingEntities()) {
                
                IEnumerable<Reserve> ras;
                ras= db.Reserves.Where(x =>
                                x.StartDateTime.CompareTo(EndDateTime) < 0
                              && x.EndDateTime.CompareTo(StartDateTime) > 0
                              && x.Room == Room 
                              && x.IsEnabled == "Y"
                              && x.Creator != Creator
                              ).ToList();
                return (ras.Count() > 0) ? true : false;
            }
        }


        public string RepeatSave(Reserve e) {
            string oldRepeatID = e.RepeatID
                , DatesList = "";
            RepeatAgenda oldRepeatAgenda = e.RepeatAgenda;

            if (e.IsEnabled == "N" && (e.IsRepeat == "on" || e.IsRepeat == "true")) {
                DeleteRepeatedReserves(oldRepeatID);     //全部先刪除再寫入
                return "Removed all repeatable reserves success.";
            }

            int iIsOldRepeat = FindExistedRepeatAgenda(e);
            if (iIsOldRepeat > 0) {
                RepeatableReserveUpdate(e, oldRepeatID);  
                return "Update Repeatalbe Reserves Success.";
            }

            RepeatAgenda ra = RepeatAgendaSave(e);  //先儲存公版的Repeat info，有了之後再做其他事情
            DeleteRepeatedReserves(oldRepeatID);     //全部先刪除再寫入
            DatesList = CreateRepeatAgendaLoopString(ra);       //產生所有連續日期的字串

            //儲存DatesList 寫到RepeateAgeda內
            using (db = new BookingEntities()) {
                var ReA = db.RepeatAgendas.Where(x => x.RepeatID == ra.RepeatID).SingleOrDefault();
                ReA.RptDatesStr = DatesList;
                db.SaveChanges();
            }

            string[] DatesArr = DatesList.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            Array.Sort(DatesArr);

            RepeatableReserveAddNew(ra, DatesArr);
            return "";
        }

        /// <summary>
        /// 詢問是否有壓到重複的會議室?
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public string IsExistReservedOnDates(Reserve e) {
            e.RepeatAgenda.RepeatID = "";
            string RepeatDates=CreateRepeatAgendaLoopString(e.RepeatAgenda),
                    DupDates="",
                    SDateTime="",
                    EDateTime="";
            string[] RDates = RepeatDates.Split(new string[] { "," },StringSplitOptions.RemoveEmptyEntries);

            using (db = new BookingEntities()) {
                foreach(var d in RDates){
                    SDateTime = d + "T" + e.StartTime + ":00Z";
                    EDateTime = d + "T" + e.EndTime + ":00Z";
                    DupDates += (this.IsReserveExisted(e.Room, SDateTime, EDateTime, e.Creator))
                        ? d + " " + e.StartTime + "~" + e.EndTime + "@"
                        : "";
                }
                return DupDates;
            }
        }

        private void RepeatableReserveAddNew(RepeatAgenda ra, string[] RepeatableDatesArr) {
            using (db = new BookingEntities()) {
                foreach (string d in RepeatableDatesArr) {

                    Reserve r = new Reserve() {
                        GUID = Guid.NewGuid(),
                        Agenda = ra.Agenda,
                        Room = ra.Room,
                        RepeatID = ra.RepeatID,
                        IsEnabled = "Y",
                        Note = ra.Note,
                        IsRepeat = "Y",
                        StartDateTime = d + "T" + ra.StartTime + ":00Z",
                        EndDateTime = d + "T" + ra.EndTime + ":00Z",
                        Creator = ra.Creator,
                        InsertDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm")
                    };
                    db.Reserves.AddObject(r);
                    db.SaveChanges();
                }
            }
        }

        private void RepeatableReserveUpdate(Reserve e, string OldRepeatID) {
            using (db = new BookingEntities()) {
                List<Reserve> Rs = db.Reserves
                    .Where(x => x.RepeatID == OldRepeatID && x.IsRepeat == "Y" && x.IsEnabled == "Y").ToList();
                foreach (var r in Rs) { 
                    r.Agenda=e.Agenda;
                    r.Note = e.Note;
                }
                db.SaveChanges();
            }
        }

        private void DeleteRepeatedReserves(string RepeatID) {
            using (db = new BookingEntities()) {
                if (!String.IsNullOrEmpty(RepeatID)) {
                    var e = db.RepeatAgendas.Where(x => x.RepeatID == RepeatID).SingleOrDefault();
                    e.IsDelete = "Y";
                    db.SaveChanges();

                    var olds = db.Reserves.Where(x => x.RepeatID == RepeatID && x.IsRepeat == "Y").ToList();
                    foreach (var old in olds) {
                        old.IsEnabled = "N";
                    }
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// 檢查是否是相同的Repeat 設定(包含設定時間、會議室)，'
        /// 只要有一個條件不符合，就重新設定Repeat
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private int FindExistedRepeatAgenda(Reserve r) {
            RepeatAgenda e = r.RepeatAgenda;
            e.RptEndAfterOccur=e.RptEndAfterOccur ?? 0;

            using (db = new BookingEntities()) {
                int i = db.RepeatAgendas.Where(x => 
                    x.StartTime == r.StartTime
                    && x.EndTime == r.EndTime
                    && x.Room==r.Room
                    && x.RptStartDate == e.RptStartDate
                    && x.RptEndDay == e.RptEndDay
                    && x.RptEndTyp == e.RptEndTyp
                    && x.RptEvery == e.RptEvery
                    && x.RptOnWkDay == e.RptOnWkDay
                    && x.RptEndAfterOccur == e.RptEndAfterOccur
                    && x.IsDelete=="N").Count();
                return i;
            }
        }
   
        /// <summary>
        ///  exec sp_RepeatDay 'Sun',2,'2014-06-25','2014-11-12',0 --操作條件範例1:用時間範圍限定
        ///  exec sp_RepeatDay 'Tue',1,'2014-06-25','',5			--操作條件範例2:用出現次數限制!
        /// </summary>
        /// <param name="ra"></param>
        private string CreateRepeatAgendaLoopString(RepeatAgenda ra) {
            string SQL="exec sp_RepeatDayStrings @WkDay, @iRepeatEvery, @StartOnDate, @EndOnDate ,@EndAfterOccur ,@RepeatID"
                ,RS = "";
            string[] wkDays = ra.RptOnWkDay.Split(new string[] { ", " }, StringSplitOptions.None);
            ra.RptEndAfterOccur = (ra.RptEndTyp == "onWhatDay") ? 0 : ra.RptEndAfterOccur;
            ra.RptEndDay = (ra.RptEndDay) ?? ra.RptStartDate;
            ra.RepeatID = (ra.RepeatID) ?? "";

            using (db = new BookingEntities()) {
                foreach (string wk in wkDays) {
                    var paras = new object[]{ 
                            new SqlParameter("@WkDay", wk),
                            new SqlParameter("@iRepeatEvery", ra.RptEvery),
                            new SqlParameter("@StartOnDate", DateTime.Parse( ra.RptStartDate)),
                            new SqlParameter("@EndOnDate",DateTime.Parse( ra.RptEndDay)),
                            new SqlParameter("@EndAfterOccur", ra.RptEndAfterOccur),
                            new SqlParameter("@RepeatID", ra.RepeatID)
                    };
                    RS += db.ExecuteStoreQuery<string>(SQL, paras).SingleOrDefault();
                }

                //var ReA=db.RepeatAgendas.Where(x => x.RepeatID == ra.RepeatID).SingleOrDefault();
                //ReA.RptDatesStr = RS;
                //db.SaveChanges();
            }
            return RS;
        }

        /// <summary>
        /// Create a new RepeatAgenda...
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private RepeatAgenda RepeatAgendaSave(Reserve e) {
            RepeatAgenda ee=e.RepeatAgenda;
            RepeatAgenda ra = new RepeatAgenda() {
                Room = e.Room
                , Note = e.Note
                , IsEnabled = "Y"
                , EndTime = e.EndTime
                , StartTime = e.StartTime
                , Creator = e.Creator
                , Agenda = e.Agenda

                , RptEndAfterOccur = ee.RptEndAfterOccur??0
                , RptEndDay = ee.RptEndDay
                , RptEndTyp = ee.RptEndTyp
                , RptEvery = ee.RptEvery
                , RptOnWkDay = ee.RptOnWkDay
                , RptStartDate = e.StartDate 
                , RptTyp = ee.RptTyp
                , Typ = ee.Typ
                , IsDelete ="N"
                , InsertDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm")
            };

            using (db = new BookingEntities()) {
                if (String.IsNullOrWhiteSpace(e.RepeatAgenda.RepeatID)) {
                    ra.RepeatID = Guid.NewGuid().ToString();
                    db.RepeatAgendas.AddObject(ra);
                } else {

                }
                db.SaveChanges();
                return ra;
            }
        }
         #endregion

        /// <summary>
        /// 一般預約設定情況
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private Reserve SaveSingleReserve(Reserve e) {
            Reserve m;
            using (db = new BookingEntities()) {
                if (e.GUID == null) {
                    e.GUID = Guid.NewGuid();
                    e.StartDateTime = e.SDateTime;
                    e.EndDateTime = e.EDateTime;
                    e.InsertDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm");
                    e.IsEnabled = "Y";
                    db.Reserves.AddObject(e);
                } else {
                    m = db.Reserves.Where(x => x.GUID == e.GUID.Value).SingleOrDefault();
                    m.Agenda = e.Agenda;
                    m.Room = e.Room;
                    m.Note = e.Note;
                    m.StartDateTime = e.SDateTime;
                    m.EndDateTime = e.EDateTime;
                    m.InsertDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm");
                    m.IsRepeat = e.IsRepeat;
                    if (e.IsEnabled == "N") { m.IsEnabled = e.IsEnabled; }
                }
                db.SaveChanges();
                return e;
            }
        }

        [HttpPost]
        public JsonResult ShowRoomList()
        {
            using (db = new BookingEntities())
            {
                var model = db.Rooms.Where(x=>x.IsDisable!="Y" || x.IsDisable==null).ToList();
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
