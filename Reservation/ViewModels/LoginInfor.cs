﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Reservation.ViewModels
{
    public class LoginInfor
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string GID { get; set; }
        public string ErrMsg { get; set; }

        public string FromSite { get; set; }
        public string DefaultRoom { get; set; }
    }
}