﻿
    //Like .net GUID()
    function newGuid() {
        var guid = "";
        for (var i = 1; i <= 32; i++) {
            var n = Math.floor(Math.random() * 16.0).toString(16);
            guid += n;
            if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
                guid += "-";
        }
        return guid;
    }

    function newUniqueID(iLength, UserName) {
        var id = "";
        for (var i = 1; i <= iLength; i++) {
            id += Math.floor(Math.random() * 32.0).toString(32);
        }
        return id;
    }


    //UniqueSessionKey with Global variables. -------------------
    var USKey;
    var BookingUser = "";
    function GetUniqueSessionID(key) 
    {
        //var key = newUniqueID(9);
        if (window.localStorage.getItem("USKey") == undefined) {
            window.localStorage["USKey"] = key;
        } else {
            if (window.localStorage["USKey"] === null) window.localStorage["USKey"] = key;
        }
        USKey = window.localStorage["USKey"];
    }
//*---------------------------------------------------------------------------------------------

   
    

(function ($) {

    $.fn.valc = function (options) {

        // Create some default values, extending them with any options that were provided
        var settings = $.extend({
            'location': 'top',
            'background-color': 'blue'
        }, options);

        return this.each(function () {

            // Tooltip plugin code here
            var v = $(this).val();
            var regex = /|/gi;
            v = v.replace(regex, "22");
            $(this).val(v);
        })
    };


    $.fn.extend({
        /*20120728 --*/
        myOpen: function (options) {
            var o = jQuery.extend({}, options);  //option settings.
            return this.each(function () {
                var me = $(this);
                var offset = me.offset();
                var height = me.height();
                var top = offset.top + height + 4;
                var left = offset.left;
                //console.log(top, left);
                $(".myddl").css({ "top": -1000, "left": -1000 }).fadeOut(1);
                $(o).css({ "top": top, "left": left }).fadeIn(600);
                $("body").append("<div id='myopenback'></div>");
                $("#myopenback").live("click", function () {
                    $(this).fadeOut(200).remove();
                    $(".myddl").css({ "top": -1000, "left": -1000 }).fadeOut(200);
                });
                //this.gMap = new GMap2(this);
                //this.gMap.setCenter(new GLatLng(o.center[0], o.center[1]), o.zoom);
            });
        }
    });

})(jQuery);




/* 上傳檔案JS模組========================================================================*/

    function fn_pub_InitialUploader(DOMID, _tempid) 
    {
        var uploader = new qq.FileUploader(
        {
            element: document.getElementById(DOMID),
            action: host("/Upload/" + DOMID.split("-")[1]),

            sizeLimit: 10 * 1024 * 1024,
            minSizeLimit: 0,
            allowedExtensions: ["jpg", "jpeg", "png", "gif"],
            params: { tempid: _tempid },
            messages:
            {
                typeError: "{file} 檔案類型錯誤. 只允許上傳以下副檔名的檔案：\r\n{extensions}.",
                sizeError: "{file} 超過檔案大小最大限制, 最大檔案大小為 {sizeLimit}.",
                minSizeError: "{file} 小於檔案大小最低限制, 最低檔案大小為 {minSizeLimit}.",
                emptyError: "{file} is empty, please select files again without it.",
                onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."
            },
            showMessage: function (message) {
                /* Need Easy UI Jquery API */
                //$.messager.alert('錯誤', message, 'error');
                alert('錯誤:'+message);
            }
            , onComplete: function (id, fileName, responseJSON) {
                fn_pub_FileUploader_onComplete(id, fileName, responseJSON, DOMID,_tempid);
            }
        });
    }


    function fn_pub_FileUploader_onComplete(id, fileName, responseJSON, DOMID, _tempid) {
        if (responseJSON.success) {
            //console.log(fileName); 
            fn_pub_ShowAttachFilesDisplay(DOMID, _tempid);
            //$.messager.alert('訊息', fileName + " 檔案上傳完成!", 'info');
        }
        else {
            $.messager.alert('錯誤', '檔案上傳錯誤', 'error', function () {
                if (responseJSON.message) {
                    $.messager.show({
                        showType: 'show',
                        title: '錯誤訊息內容',
                        msg: responseJSON.message,
                        timeout: 0,
                        width: 600,
                        height: 200
                    });
                }
            });
        }
    }

    var imgForTempID;
    function fn_pub_ShowAttachFilesDisplay(DOMID, _tempid) {
        var f = DOMID.split("-")[1];
        imgForTempID = _tempid;
        $.post(host("/Upload/ShowAttachFiles/" + f + "/" + _tempid), {}, function (rs) {
            //alert("#" + DOMID + " .p-atf");
            $("#show_room").remove();
            $("#" + DOMID).after(rs);
        });
    }

    function fn_pub_deleteAttachFile(e, typ, guid)
    {
        var me = $(e).parent();
        var sn = me.data("sn");
        //$.messager.alert('todo...', 'remove ' + sn, 'infor');
        if (confirm("確定要刪除該檔案?")) 
        {
            var obj={fileID:sn};
            //console.log("../Upload/DeleteAttachFile/" + _tempid);
            var div_id = $(".qq-uploader").parent().attr("id");
            $.post(host("/Upload/DeleteAttachFile/" + typ + "/" + guid), obj, function () {
                fn_pub_ShowAttachFilesDisplay(div_id, imgForTempID);
            });
        }
    }

    /* END　上傳檔案JS模組========================================================================*/

/*附屬表單 Submenu*/
    function fn_pub_SubMenu(id, e) {
        var height = $(e).height();
        var offset = $(e).offset();
        var top = offset.top + height + 2;
        var left = offset.left;
        $("#" + id).css({ top: top, left: left }).show().animate({ opacity: 1 }, 500);
    }


    //可在Javascript中使用如同C#中的string.format
    //使用方式 : var fullName = String.format('Hello. My name is {0} {1}.', 'FirstName', 'LastName');
    String.format = function () {
        var s = arguments[0];
        if (s == null) return "";
        for (var i = 0; i < arguments.length - 1; i++) {
            var reg = getStringFormatPlaceHolderRegEx(i);
            s = s.replace(reg, (arguments[i + 1] == null ? "" : arguments[i + 1]));
        }
        return cleanStringFormatResult(s);
    }
    //可在Javascript中使用如同C#中的string.format (對jQuery String的擴充方法)
    //使用方式 : var fullName = 'Hello. My name is {0} {1}.'.format('FirstName', 'LastName');
    String.prototype.format = function () {
        var txt = this.toString();
        for (var i = 0; i < arguments.length; i++) {
            var exp = getStringFormatPlaceHolderRegEx(i);
            txt = txt.replace(exp, (arguments[i] == null ? "" : arguments[i]));
        }
        return cleanStringFormatResult(txt);
    }
    //讓輸入的字串可以包含{}
    function getStringFormatPlaceHolderRegEx(placeHolderIndex) {
        return new RegExp('({)?\\{' + placeHolderIndex + '\\}(?!})', 'gm')
    }
    //當format格式有多餘的position時，就不會將多餘的position輸出
    //ex:
    // var fullName = 'Hello. My name is {0} {1} {2}.'.format('firstName', 'lastName');
    // 輸出的 fullName 為 'firstName lastName', 而不會是 'firstName lastName {2}'
    function cleanStringFormatResult(txt) {
        if (txt == null) return "";
        return txt.replace(getStringFormatPlaceHolderRegEx("\\d+"), "");
    }
