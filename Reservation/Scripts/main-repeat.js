﻿//----------------------------------------------------------
function fn_init_ddl_rpts() {
    var ss = "";
    var arr = ["weekly"];
    for (var i in arr) { ss += "<option>" + arr[i] + "</option>" };
    $("#ddl_rpts").html(ss);
}

function fn_init_ddl_rpt_every(irptee) {
    var ss = "";
    for (var i = 1; i < irptee; ++i) { ss += "<option>" + i + "</option>" };
    $("#ddl_rpt_every").html(ss);
    $("#rpt_every_sp").html("weeks");
}

function fn_init_cks_rpt_on() {
    var ss = "" ,arr = ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"];
    var tmp = "<label class='lb_chk_rpt_on {0}'><input type='checkbox' data-summary='{0}' />{0}</label>";

    for (var s in arr) { ss += String.format(tmp, arr[s]); }
    $("#cks_rpt_on").html(ss);
}

function fn_init_input_ends_on() {
    var ss = "";
    for (var i = 1; i <= 40; ++i) { ss += "<option>" + i + "</option>" };

    $("#after_occurs").html(ss);
    var RA_RptEndAfterOccur = $("#after_occurs").data("v");
    $("#after_occurs").val(RA_RptEndAfterOccur);

    $("#endson_whatday").datepicker({ dateFormat: datetime_format });
    $("input[name='e.RepeatAgenda.RptEndTyp']:eq(0)").click();
}

$(".eIsRepeat").click(function () {
    //$(".rpt_table").toggle();
    var isChecked = $(this).prop("checked");
    $(this).val((isChecked));
    if (!isChecked) {
        $("span.summary").html(($("#e_RepeatID").val() != "") ? "確定解除重複設定?" : "");
        $(".rpt_table").hide();
    } else {
        fn_RepeatSummary();
        $(".rpt_table").show();
    }
});

function fn_RepeatSummary() {

    /*如果IsRepeat沒有勾選就都不用Summary*/
    if (!$(".eIsRepeat").prop("checked")) return "";

    var ss = ""
        , smm="summary"
        , ddl_rpts = $(".rpt_table #ddl_rpts").data(smm)
        , ddl_rpt_every_v = $(".rpt_table #ddl_rpt_every").val()
        , ddl_rpt_every = $(".rpt_table #ddl_rpt_every").data(smm).replace("{}", ddl_rpt_every_v)
        /*, cks_rpt_on = $.map($("#cks_rpt_on input:checked"), function (n, i) { return $(n).data(smm); }).join(", ")*/
        , cks_rpt_on = $("#hidden_rpt_on").val()
        , start_on_v = $("#input_starts_on").val()
        , start_on = $("#input_starts_on").data(smm).replace("{}", start_on_v)
        , rdo_end_v = $("input[name='e.RepeatAgenda.RptEndTyp']:checked").val()
        , endson_whatday_v = $("#endson_whatday").val()
        , endson_whatday = $("#endson_whatday").data(smm).replace("{}", endson_whatday_v)
        , after_occurs_v = $("#after_occurs").val()
        , after_occurs = $("#after_occurs").data(smm).replace("{}", after_occurs_v)
        /*, rdo_recurTyp = $("input[name='e.RepeatAgenda.RecurTyp']:checked").data(smm)*/
        ;
    ss = start_on;
    ss += (ddl_rpt_every_v == "1")
            ? ddl_rpts.replace("{}", cks_rpt_on)
            : ddl_rpt_every.replace("{0}", ddl_rpt_every_v).replace("{1}", cks_rpt_on);
    ss += (rdo_end_v === "Occurences") ? after_occurs : "";
    ss += (endson_whatday_v != "" && rdo_end_v === "onWhatDay") ? endson_whatday : "";
    /*ss += rdo_recurTyp;*/

    $("span.summary").html(ss);
    /*$("#hidden_rpt_on").val(cks_rpt_on);*/
    return ss;
}
/* 被封鎖起來 =====
#cks_rpt_on input, 
input[name='e.RepeatAgenda.RecurTyp']
 */
$("#FormPanel").on("change", "#StartDate, #ddl_rpt_every, input[name='e.RepeatAgenda.RptEndTyp'], #endson_whatday, #after_occurs ", function () {
    fn_RepeatSummary();
});

$("input[name='e.RepeatAgenda.RptEndTyp']").click(function () {
    $("#after_occurs,#endson_whatday").attr("disabled", "disabled").addClass("disabled");
    $(this).parent().find(".ends_on").removeAttr("disabled").removeClass("disabled");
});
