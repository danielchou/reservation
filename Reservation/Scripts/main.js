﻿var calendar;
var debug=false;
var datetime_format = "yy-mm-dd";
var rmval = $("#RoomList").data("selectd-value");
var roomid = (rmval === "") ? "All" : rmval;
if (roomid == "All") { $("#addnewbooking").hide(); } else { $("#addnewbooking").show(); }
var evpanel = "<div class='ev-panel'><div class='agenda' data-guid='{3}' data-roomid='{0}'>{5}{4}{1}</div><div class=creator>{2}</div></div>";
var EditableInput = "#RoomList,#Agenda,#StartDate,#StartTime,#EndDate,#EndTime,#Note, .eIsRepeat";
var WkDayArr = new Array("Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat");

$(function () {
    fnCheckLoginUser();
    iniCalendar();
    ResizeCalendar();
    $("#myModalLabel").html("新預約會議室");
    $(".group-sn").hide();
    $("#StartDate").datepicker({
        dateFormat: datetime_format,
        minDate: 0,
        onClose: function (selectedDate) {
            var v = $(this).val();
            $("#EndDate").datepicker({
                dateFormat: datetime_format,
                "minDate": selectedDate
            }).val(v);
            $("#EndDate").val(v);
            /* 將Repeat起始日期設定為StartDate */
            $("#input_starts_on").val(v);
            /* 自動將計算星期幾代入 hidden 中*/
            var day = new Date(Date.parse(v.replace(/-/g, '/')));
            var wkDay = WkDayArr[day.getDay()];
            $("#hidden_rpt_on").val(wkDay + "");
            fn_RepeatSummary();
        }
    });
    $("#StartTime,#EndTime").html(fnHHMM(9));
    /* //$("#myModal").modal('show');  //Showup the dialog at beginning...*/


    if ($("#errorMsg").html() !== "") {
        /*跳出錯誤訊息，打開編輯窗*/
        $("#errorMsg").show();
        $("#myModal").modal('show');
        fnReversSelVal("#RoomList");
        /*fnReversSelVal("#StartTime");*/
        /*fnReversSelVal("#EndTime");*/
        $("#StartTime").val($("#StartTime").data("selectd-value") + "");
        $("#EndTime").val($("#EndTime").data("selectd-value") + "");

        //----------------------------------------------------
        var err = $("#errorMsg").html();
        var err2 = err.replace(/@/g, "<br />");
        $("#errorMsg").html(err2);


        var isRepeat = $(".eIsRepeat").data("isrepeat");
        if (isRepeat == true || isRepeat == "on") {
            $(".eIsRepeat").prop("checked", true);
            $(".rpt_table").show();
        }
        var RA_RptEndTyp = $(".rdo_list").data("v");
        var RA_RptEndDay = $("#endson_whatday").data("v");

        $("input[value='" + RA_RptEndTyp + "']").prop("checked", true);
        $("#endson_whatday").val(RA_RptEndDay);
    } else {
        $("#errorMsg").hide();
    }

    fnShowRoomList();

    fn_init_ddl_rpts();
    fn_init_ddl_rpt_every(15);
    fn_init_input_ends_on();
    /*fn_init_cks_rpt_on(); //關閉*/
    $("input[name='e.RepeatAgenda.RecurTyp']:first").prop("checked", true);
    //$("#OnelabMeetingRoomMap").click();
    
});

var ihp = -1;
$("#help,#helpClose").click(function () {
    ihp = ihp * -1; if (ihp > 0) { $("#helpPanel").fadeIn("fast"); } else { $("#helpPanel").hide(); }
    ResizeCalendar();
});

function fnReversSelVal(s) {
    $(s).val($(s).data("selectd-value")+"");
}

function fnHHMM(startHr) {
    var hh = "", rs = "", tmp = "<option value='{0}:00'>{0}:00</option><option value='{0}:30'>{0}:30</option>";
    for (var i = startHr; i < 22; i++) {
        hh = (i < 10) ? "0" + i : i;
        rs += String.format(tmp, hh);
    }
    return rs;
}


function fnShowRoomList() {
    $.post(host("MeetingRoom/ShowRoomList"), {}, function (rs) {
        var ss = "";
        for (var c in rs) { if (rs[c].Name != "All") { ss += String.format("<option value='{0}'>{0}</option>", rs[c].Name); } }
        $("#RoomList").html(ss);
        $("#RoomTab li:eq(0)").css("border-bottom", "solid 3px #fff").click();
        fnReversSelVal("#RoomList");

        $("#RoomTab").on("click", "li", function () {
            var me = $(this);
            var r = "f" + $(me).attr("title").split("")[0];
            $("#RoomTab li").css("border-bottom", "solid 0px #fff");
            $(me).css("border-bottom", "solid 7px #fff");
            $("header").removeAttr("class").addClass(r);
            roomid = me.find("b").html();
            fnChangeCalData(roomid);
            if (roomid == "All") {
                $("#addnewbooking, .deviceDscr").hide();
            } else {
                $(".deviceDscr").hide();
                $("#addnewbooking").show();
                var arr = $(".deviceDscr[data-room='" + roomid + "']").attr("data-Device").split("|");
                var ss = "<b>Available Device：</b><ol><li>" + arr.join("</li><li>") + "</li></ol>";
                $(".deviceDscr[data-room='" + roomid + "']").html(ss).show();
            }
        })
        .fadeIn(500);
        /*$("#RoomTab li").css("border", "solid 1px #ddd");*/
        if (defaultRoom != "") { roomid = defaultRoom; }
        $("#RoomTab li[title='" + roomid + "']").css("border-bottom", "solid 2px #fff").click();
        DisabledRoom = DisabledRoom.substr(0, DisabledRoom.length - 1);
        $(DisabledRoom).hide();

    });
}

function iniCalendar() {
    calendar = $('#calendar').html("").fullCalendar({
        header: { left: 'prev,today,next', center: 'title', right: '' },
        defaultView: 'agendaWeek',
        axisFormat: 'h(:mm)tt',
        allDaySlot: false,
        agenda: 'hh:mm{-hh:mm}', /* 5:00 - 6:30 */
        minTime: '09:00',
        maxTime: '21:00',
        slotMinutes: 30,
        weekends: true,
        editable: false,
        titleFormat: {
            month: 'yyyy-MM',
            week: "yyyy-MM-dd{ '~' MM-dd}",
            day: 'yyyy-MM-d dddd'
        },
        dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat'],
        events:
            {
                url: host("MeetingRoom/ShowReservation"),
                type: "post",
                data: { roomid: roomid },
                error: function () {
                    //console.log("error", roomid);
                    //alert("Error! " + host("MeetingRoom/ShowReservation"));
                }
            },
        eventRender: function (e, element) {
            /*console.log(roomid,LoginUser);*/
            var a = e.title.split("<b>")
                    , guid = a[1]
                    , creator = a[2]
                    , room = a[3]
                    , roomlb = ""
                    , agenda = a[4]
                    , note = a[5]
                    , isRepeat = a[6]
                    , panelTmp = evpanel;
            isRepeat = (a[6] == "") ? "" : "<b class='rpt-mark'>" + a[6] + "</b>";
            isRepeat = (roomid == "All") ? "" : isRepeat;
            //console.log(room, roomlb);
            roomlb = (roomid == "All") ? "<b class='room-lb'>"+room+"</b>" : "";
            ee = String.format(panelTmp, room, agenda, creator, guid, isRepeat, roomlb);
            element.find(".fc-event-inner").append(ee);
        }

    });
    $("header").append($(".btns"));
    $(".fc-button-next").after($("#addnewbooking"));
}

$(window).resize(function () { ResizeCalendar(); });

function ResizeCalendar() {
    var h1 = $(window).height();
    $('#calendar').fullCalendar('option', 'height', h1 - 105);
}


function fnChangeCalData(_roomid) {
    var events = {
        url: host("MeetingRoom/ShowReservation"),
        type: 'POST',
        data: { roomid: _roomid }
    };
    $('#calendar').fullCalendar('removeEventSource', events);
    $('#calendar').fullCalendar('addEventSource', events);
    $('#calendar').fullCalendar('refetchEvents');
}

var LoginUser = "";
function fnCheckLoginUser() {
    if (window.localStorage.BookingUser === undefined) {
        //location.href = ITSUrl;
    }
    else {
        LoginUser = window.localStorage.BookingUser;
        $("#e_Creator").val(LoginUser);
        $("#BookingUser b").html(LoginUser);
    }
}

$("#addnewbooking").click(function () {
    $(".modal-footer").show();
    $(".isRepeatable, .summary").show();
    fnClear();
});

function fnClear() {
    $(EditableInput).removeAttr("disabled");
    /*$("#RoomList").attr("disabled","disabled");*/
    $("#RoomList").val(roomid);
    $("#e_guid,#Agenda,#Note,#StartDate,#EndDate").val("");
    $("#txtStartDateTime,#txtEndDateTime").val(fnTodayDate());
    $("#StartTime,#EndTime").val("09:00");
    $("#lbCreator").html(LoginUser);
    $("#errorMsg").hide();

    /* Repeat controller initialized */
    $("#e_RepeatID, #hidden_rpt_on, #input_starts_on, #endson_whatday").val("");
    $(".summary").html("");
    $(".eIsRepeat").prop("checked", false);
    $(".rpt_table").hide();
    $("#ddl_rpt_every, #after_occurs").val("1");
    $("input[name='e.RepeatAgenda.RptEndTyp']:first").prop("checked", true);
}

function fnTodayDate() {
    var dt = new Date(), imm, idd, mm, dd;
    imm = dt.getMonth() + 1;
    idd = dt.getDate();
    mm = (imm < 10) ? "0" + imm : "" + imm;
    dd = (idd < 10) ? "0" + idd : "" + idd;
    return dt.getFullYear() + "-" + mm + "-" + dd;
}

$("#btnClear").click(function (e) {
    e.preventDefault();
    fnClear();
});

$("#btnRemove").click(function () {
    var deleteDscr=$(".eIsRepeat").prop("checked")
        ?"確定要刪除所有重複顯示?"
        :"確定要刪除該筆預約紀錄?";
    if (confirm(deleteDscr)) {
        $("#e_IsEnabled").val("N");
    }
});

$("#calendar").on("click", ".fc-event-inner", function () {
    var gid = $(this).find(".agenda").data("guid");
    $.post(host("MeetingRoom/GetOneReservation"), { guid: gid }, function (rs) {
        $("#myModal").modal('show');
        var o = rs.Reserve;
        var sarr = o.StartDateTime.split("T");
        var earr = o.EndDateTime.split("T");

        $("#e_guid").val(o.GUID);
        $("#RoomList").val(o.Room);
        $("#Agenda").val(htmlDecode(o.Agenda));
        $("#Note").val(htmlDecode(o.Note));
        $("#StartDate").val(sarr[0]);
        $("#StartTime").val(sarr[1].substr(0, 5));
        $("#EndDate").val(earr[0]);
        $("#EndTime").val(earr[1].substr(0, 5));
        $("#lbCreator").html(o.Creator);

        $("#errorMsg").html("");

        /*--Repeat information data binding. -------------------------------------*/
        $("#e_RepeatID").val(o.RepeatID);

        if (rs.Repeat != null) {
            var rp = rs.Repeat;
            $(".isRepeatable, .summary").show();
            if (o.IsRepeat == "Y") {
                $(".eIsRepeat").prop("checked", true);
                $(".rpt_table").show();
            }

            /*$("#a_starts_on").html($("#StartDate").val());*/
            $("#ddl_rpt_every").val(rp.RptEvery);
            $("#ddl_rpts").val(rp.RptTyp);
            var wksArr = rp.RptOnWkDay.split(", ");
            /* $("#cks_rpt_on label input").each(function (i, n) { $(n).prop("checked", false); });*/
            /* for (var i in wksArr) { $("#cks_rpt_on label." + wksArr[i]).find("input").prop("checked", true);} */
            $("#hidden_rpt_on").val(rp.RptOnWkDay);
            $("#input_starts_on").val(rp.RptStartDate);
            $("input[value='" + rp.RptEndTyp + "']").prop("checked", true).click();
            $("#after_occurs").val(rp.RptEndAfterOccur);
            $("#endson_whatday").val(rp.RptEndDay);
            fn_RepeatSummary();
        } else {
            $(".isRepeatable, .summary").hide();
        }
        /*------------------------------*/
        if (LoginUser.toLowerCase() == o.Creator.toLowerCase()) {
            $(".modal-footer").show();
            $(EditableInput).removeAttr("disabled");
        } else {
            if (!debug) {
                $(".modal-footer").hide();
                $(EditableInput).attr("disabled", "disabled");
            }
            $(".rpt_table").hide();
        }
    });

});


function htmlDecode(value) {
    if (value) {
        return $('<div />').html(value).text();
    } else {
        return '';
    }
}

$("#MeetingRoomMap .room").click(function () {
    var v = $(this).data("room");
    $(this).parent().parent().hide();
    $("#RoomTab li[title='" + v + "']").click();
});
$("#OnelabMeetingRoomMap").click(function () { $("#MeetingRoomMap").fadeIn("fast"); });
$("#btnCloseMap").click(function () { $("#MeetingRoomMap").hide(); });

$("#FormPanel").submit(function (event) {
    var startDate = $("#StartDate").val();
    //alert("Todo:攔截不可預約的會議室!提前給你警告" + startDate + "!!");
    if (startDate == "") {
        alert("請輸入預約時間，從小月曆內選取!");
        event.preventDefault();
    }
});

$("#FormPanel").on("click", "#StartTime", function () {
    var v = $(this).val();
    var k = 0;
    $("#StartTime option").each(function (i, n) { if ($(n).val() === v) k = i; });
    var all = $("#StartTime option").size();
    k = (all == k) ? k-1 : k + 1;
    
    if ($("#StartDate").val() === $("#EndDate").val() && k!=all) {
        var vv = $("#EndTime option:eq(" + k + ")").val();
        $("#EndTime").val(vv);
        //console.log(v, all, vv, k);
    }
});