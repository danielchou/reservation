﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Reservation.Models;
using System.Web.Mvc;

namespace Reservation.Models
{
    [MetadataType(typeof(ReserveMetadata))]
    public partial class Reserve
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ErrorMsg { get; set; }

        public string email { get; set; }
        public string gid { get; set; }

        private class ReserveMetadata
        {
            [Required]
            public string StartDate { get; set; }
            [Required]
            public string EndDate { get; set; }
            //加了這個才不會被擋住!
            [AllowHtml]
            public string Note { get; set; }
            [AllowHtml]
            public string Agenda { get; set; }
        }
        /// <summary>
        /// 客戶端傳送的起始日期時間
        /// </summary>
        public string SDateTime { get { return StartDate + "T" + StartTime + ":00Z"; } }
        /// <summary>
        /// 客戶端傳送的結束日期時間
        /// </summary>
        public string EDateTime { get { return EndDate + "T" + EndTime + ":00Z"; } }
        public RepeatAgenda RepeatAgenda { get; set; }

        public string RA_RptOnWkDay { get; set; }
        public string RA_RptStartDate { get; set; }
        public int? RA_RptEvery { get; set; }
        public string RA_RptEndTyp { get; set; }
        public int? RA_RptEndAfterOccur { get; set; }
        public string RA_RptEndDay { get; set; }
    }
}