﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Reservation.Models;

namespace Reservation.Attritube
{

    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }

    public class TestAttr : ActionFilterAttribute 
    {
        public string email { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            var dd = filterContext.ActionParameters["e"];
            Reserve e = (Reserve)dd;
            string email = (e == null) ? "" : e.email;
            string gid = (e == null) ? "" : e.gid;
            string url = "http://localhost:26924/IT_Service_Portal/Home/Home.aspx?gid=" + gid;

            if (String.IsNullOrEmpty(email))
            {
                filterContext.HttpContext.Response.Redirect(url);
            }
            else
            {
                
                //filterContext.Result = new RedirectToRouteResult(
                //    new RouteValueDictionary{
                //      {"controller","MeetingRoom"},
                //      {"action","Test"},
                //      {"returnUrl", filterContext.HttpContext.Request.RawUrl }
                //    });
            }
        }    
    }
}